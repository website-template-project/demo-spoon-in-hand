/* URL
 * A class for manipulating urls
 *
 * Note: only takes the first value of a param
 */

class URL {
    constructor(url) {
        let hashParts = url.split("#"),
            urlParts = hashParts[0].split("?"),
            queryParams = []
        this.fullPath = urlParts[0]
        this.protocol = this.fullPath.split(":")[0]
        const pathParts = this.fullPath.split("/")
        this.domain = pathParts[2]
        this.relativePath = pathParts.length > 3 ? `/${pathParts.slice(3).join("/")}` : "/"
        this.params = new Map()
        this.hash = hashParts.length > 1 ? `#${hashParts[1]}` : ""
        if (urlParts.length > 1) {
            queryParams = urlParts[1].split("&")
            for (let param of queryParams) {
                param = param.split("=")
                if (this.params.get(param[0]) === undefined) {
                    this.params.set(param[0], param.length > 1 ? param[1] : undefined)
                }
            }
        }
    }

    // Use getters and setters for params map. No need to wrap
    
    getParamStr() {
        let paramStr = ""
        if (this.params.size > 0) {
            let paramArr = []
            for (let item of this.params.entries()) {
                paramArr.push(item[1] === undefined ? item[0] : `${item[0]}=${item[1]}`)
            }
            paramStr = `?${paramArr.join("&")}`
        }
        return paramStr
    }

    getFull() {
        return `${this.fullPath}${this.getParamStr()}${this.hash}`
    }
}

export default URL
