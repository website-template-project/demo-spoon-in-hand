/* Query specifically for use with gatsby-node.js
 *
 * Usage:
 *   const getLocalItemData = require("./src/util/local-item-queries").getLocalItemData
 *   const stripeData = await getLocalItemData(graphql)
 *
 * For no local items, replace contents of this file with:
 *   exports.getLocalItemData = async () => {data: {allItemsYaml: {nodes: []}}}
 */

exports.getLocalItemData = async graphql => graphql(`
  query {
    allItemsYaml {
      nodes {
        id
        name
        description
        keywords
        images
        price
        data
        currency
        fields {
          path
        }
      }
    }
  }
`)
