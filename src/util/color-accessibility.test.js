/* color-accessibility.test.js
 *
 * Get contrast ratio independently:
 *   https://juicystudio.com/services/luminositycontrastratio.php#specify
 */

import {
  relativeLuminance,
  contrastRatio,
  scaleLuminance,
  scaleContrast,
} from "./color-accessibility"

/*******************************************************************************
 * Test relativeLuminance
*******************************************************************************/

test("Relative luminance works for white", () => {
  const color = [255, 255, 255]
  expect(relativeLuminance(color)).toBeCloseTo(1.0)
})

test("Relative luminance works for black", () => {
  const color = [0, 0, 0]
  expect(relativeLuminance(color)).toBeCloseTo(0.0)
})

test("Relative luminance works for intermediate", () => {
  const color = [111, 222, 123]
  expect(relativeLuminance(color)).toBeCloseTo(0.5705211997187923)
})

test("Relative luminance works for some zero", () => {
  const color = [111, 0, 222]
  expect(relativeLuminance(color)).toBeCloseTo(0.08653433896846671)
})

test("Relative luminance works for grey", () => {
  const color = [187, 187, 187]
  expect(relativeLuminance(color)).toBeCloseTo(0.5)
})

test("Relative luminance works for darkened aquamarine", () => {
  const color = [67, 136, 112]
  expect(relativeLuminance(color)).toBeCloseTo(0.2)
})

test("Relative luminance throws error for no input", () => {
  expect(() =>
    relativeLuminance()
  ).toThrow(
    "undefined is not an array of RGB integer values in 0-255"
  )
})

test("Relative luminance throws error for input too long", () => {
  expect(() =>
    relativeLuminance([1, 2, 3, 4])
  ).toThrow(
    "1,2,3,4 is not an array of RGB integer values in 0-255"
  )
})

test("Relative luminance throws error for negative integer input", () => {
  expect(() =>
    relativeLuminance([-1, 0, 0])
  ).toThrow(
    "-1,0,0 is not an array of RGB integer values in 0-255"
  )
})

test("Relative luminance throws error for input too large", () => {
  expect(() =>
    relativeLuminance([256, 0, 0])
  ).toThrow(
    "256,0,0 is not an array of RGB integer values in 0-255"
  )
})

test("Relative luminance throws error for non-number input", () => {
  expect(() =>
    relativeLuminance([0, "a", 0])
  ).toThrow(
    "0,a,0 is not an array of RGB integer values in 0-255"
  )
})

test("Relative luminance throws error for non-integer", () => {
  expect(() =>
    relativeLuminance([0, 0, 3.5])
  ).toThrow(
    "0,0,3.5 is not an array of RGB integer values in 0-255"
  )
})

/*******************************************************************************
 * Test contrastRatio
*******************************************************************************/

test("Contrast ratio for black on white", () => {
  const fg = [255, 255, 255]
  const bg = [0, 0, 0]
  expect(contrastRatio(fg, bg)).toBeCloseTo(21.0)
})

test("Contrast ratio for white on black", () => {
  const fg = [0, 0, 0]
  const bg = [255, 255, 255]
  expect(contrastRatio(fg, bg)).toBeCloseTo(21.0)
})

test("Contrast ratio between greys", () => {
  const fg = [34, 34, 34]
  const bg = [238, 238, 238]
  expect(contrastRatio(fg, bg)).toBeCloseTo(13.71)
})

test("Contrast ratio for red on aquamarine", () => {
  const fg = [255, 0, 0]
  const bg = [127, 255, 212]
  expect(contrastRatio(fg, bg)).toBeCloseTo(3.27)
})

test("Contrast ratio throws error for bad first input", () => {
  expect(() =>
    contrastRatio(undefined, [1, 2, 3])
  ).toThrow(
    "undefined is not an array of RGB integer values in 0-255"
  )
})

test("Contrast ratio throws error for bad second input", () => {
  expect(() =>
    contrastRatio([1, 2, 3], [1, "foo", 3])
  ).toThrow(
    "1,foo,3 is not an array of RGB integer values in 0-255"
  )
})

/*******************************************************************************
 * Test scaleLuminance
*******************************************************************************/

test("Scale luminance turns white to grey", () => {
  const rgb = [255, 255, 255]
  const targetLuminance = 0.5
  const scaled = scaleLuminance(rgb, targetLuminance)
  expect(scaled).toEqual([187, 187, 187])
})

test("Scale luminance turns black to grey", () => {
  const rgb = [0, 0, 0]
  const targetLuminance = 0.5
  const scaled = scaleLuminance(rgb, targetLuminance)
  expect(scaled).toEqual([187, 187, 187])
})

test("Scale luminance darkens aquamarine", () => {
  const rgb = [127, 255, 212]
  const targetLuminance = 0.2
  const scaled = scaleLuminance(rgb, targetLuminance)
  expect(scaled).toEqual([67, 136, 112])
})

test("Scale luminance darkens green", () => {
  const rgb = [0, 200, 0]
  const targetLuminance = 0.1
  const scaled = scaleLuminance(rgb, targetLuminance)
  expect(scaled).toEqual([0, 104, 0])
})

test("Scale luminance throws error for bad rgb", () => {
  expect(() =>
    scaleLuminance([1, "foo", 3], 0.5)
  ).toThrow(
    "1,foo,3 is not an array of RGB integer values in 0-255"
  )
})

test("Scale luminance throws error for negative luminance", () => {
  expect(() =>
    scaleLuminance([1, 2, 3], -0.5)
  ).toThrow(
    "-0.5 is not a number between 0 and 1"
  )
})

test("Scale luminance throws error for excessive luminance", () => {
  expect(() =>
    scaleLuminance([1, 2, 3], 2.3)
  ).toThrow(
    "2.3 is not a number between 0 and 1"
  )
})

/*******************************************************************************
 * Test scaleContrast
*******************************************************************************/

test("Scale contrast darkens grey", () => {
  const [bg, fg] = [[242, 242, 242], [127, 127, 127]]
  const targetContrast = 4.5
  const [newBg, newFg] = scaleContrast(bg, fg, targetContrast)
  expect(newBg).toEqual([242, 242, 242])
  expect(newFg).toEqual([110, 110, 110])
})

test("Scale contrast darkens green", () => {
  const [bg, fg] = [[242, 242, 242], [0, 200, 0]]
  const targetContrast = 7
  const [newBg, newFg] = scaleContrast(bg, fg, targetContrast)
  expect(newBg).toEqual([242, 242, 242])
  expect(newFg).toEqual([0, 96, 0])
})

test("Scale contrast lightens aquamarine and darkens orange", () => {
  const [bg, fg] = [[127, 255, 212], [255, 69, 0]]
  const targetContrast = 7.0
  const [newBg, newFg] = scaleContrast(bg, fg, targetContrast, 0.5)
  expect(newBg).toEqual([238, 255, 249])
  expect(newFg).toEqual([163, 42, 0])
})

test("Scale contrast lightens aquamarine and darkens orange when reversed", () => {
  const [bg, fg] = [[255, 69, 0], [127, 255, 212]]
  const targetContrast = 7.0
  const [newBg, newFg] = scaleContrast(bg, fg, targetContrast, 0.5)
  expect(newBg).toEqual([163, 42, 0])
  expect(newFg).toEqual([238, 255, 249])
})

test("Scale contrast to 7.0 splits both grey given scale weight 0.5", () => {
  const [bg, fg] = [[127, 127, 127], [127, 127, 127]]
  const targetContrast = 7.0
  const [newBg, newFg] = scaleContrast(bg, fg, targetContrast, 0.5)
  expect(newBg).toEqual([171, 171, 171])
  expect(newFg).toEqual([33, 33, 33])
})

test("Scale contrast throws error for impossible input", () => {
  expect(() =>
    scaleContrast([127, 127, 127], [127, 127, 127], 21.0)
  ).toThrow(
    "Unable to generate colors with the given params. " +
    "Validation indicates it's possible for some values of scaleFirst. " +
    "Note that adjusting scaleFirst will affect both output colors. " +
    "rgb1: 127,127,127 rgb2: 127,127,127 targetContrast: 21 scaleFirst: 0"
  )
})

test("Scale contrast throws error for bad first color", () => {
  expect(() =>
    scaleContrast([1, "foo", 3], [1, 2, 3], 4.5)
  ).toThrow(
    "1,foo,3 is not an array of RGB integer values in 0-255"
  )
})

test("Scale contrast throws error for bad second color", () => {
  expect(() =>
    scaleContrast([1, 2, 3], [1, "foo", 3], 4.5)
  ).toThrow(
    "1,foo,3 is not an array of RGB integer values in 0-255"
  )
})

test("Scale contrast throws error for bad contrast", () => {
  expect(() =>
    scaleContrast([1, 2, 3], [1, 2, 3], -1.0)
  ).toThrow(
    "-1 is not a number between 0 and 21"
  )
})

test("Scale contrast throws error for bad scale weight", () => {
  expect(() =>
    scaleContrast([1, 2, 3], [1, 2, 3], 4.5, 3)
  ).toThrow(
    "3 is not a number between 0 and 1"
  )
})
