import moveFieldsToMessage from "./form-manipulation"

test("A single field is moved into the message", () => {
    let data = new Map()
    data.set("name", "Foo")
    data.set("happiness", "Yes")
    data.set("message", "Existing message")
    let fields = ["happiness"]
    moveFieldsToMessage(data, fields)
    expect(data.size).toEqual(2)
    expect(data.get("name")).toEqual("Foo")
    expect(data.get("message")).toEqual("happiness: Yes\n\nExisting message")
});

test("Multiple fields are moved into the message", () => {
    let data = new Map()
    data.set("name", "Foo")
    data.set("happiness", "Yes")
    data.set("message", "Existing message")
    let fields = ["name", "happiness"]
    moveFieldsToMessage(data, fields)
    expect(data.size).toEqual(1)
    expect(data.get("message")).toEqual(
      "name: Foo\n\nhappiness: Yes\n\nExisting message"
    )
});

test("An empty field is moved into the message", () => {
    let data = new Map()
    data.set("name", "Foo")
    data.set("happiness", null)
    data.set("message", "Existing message")
    let fields = ["happiness"]
    moveFieldsToMessage(data, fields)
    expect(data.size).toEqual(2)
    expect(data.get("name")).toEqual("Foo")
    expect(data.get("message")).toEqual("happiness: \n\nExisting message")
});

test("A message field is created if there isn't one", () => {
    let data = new Map()
    data.set("name", "Foo")
    data.set("happiness", "Yes")
    let fields = ["happiness"]
    moveFieldsToMessage(data, fields)
    expect(data.size).toEqual(2)
    expect(data.get("name")).toEqual("Foo")
    expect(data.get("message")).toEqual("happiness: Yes\n\n")
});

