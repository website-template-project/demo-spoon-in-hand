/* StripeProducts
 *
 *  Fetches data and passes it to the products component
 *
 *  Props:
 *    purchase (defaults to global setting): Boolean for including purchase actions
 *      If using local products and selling via stripe, product IDs must match Stripe
 *    filter (optional): function taking in a product (map) and returning a Boolean
 *
 *  Examples:
 *    <StripeProducts />
 *    <StripeProducts
 *      purchase={true}
 *      filter={product => product.keywords.includes("entree")}
 *    />
 */

import React from "react"
import useStripeItems from "../hooks/use-stripe-items"

import Products from "./products"

export default function StripeProducts(props) {

  const items = useStripeItems()
  const filteredItems = items.filter(props.filter)

  return (
    <Products
      purchase={props.purchase}
      items={filteredItems}
      data-qa={props["data-qa"] || "StripeProducts"}
    />
  )
}

StripeProducts.defaultProps = {
  filter: product => true,
}
