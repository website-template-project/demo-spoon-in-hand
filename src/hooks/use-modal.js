/* useModal
 */

import { useContext } from "react"
import ModalContext from "../util/modal-context"

export default function useModal() {
  return useContext(ModalContext)
}
