# Website template project

This project is created and supported by [Open Social Resources][osr_website].
Check out [the project website][wtp_website] to see this project in action.
Contact us using the contact form on [the website][wtp_website].

## Changes

See the [Changelog][changelog].

## Code of Conduct

See the [Code of Conduct][code_of_conduct].

## Customizing your own website

For this section, you'll need some technical expertise.

### Technology

The website template uses [Gatsby][gatsby],
which we use to build static sites with React.

### Local setup

1. Duplicate this repo into your own project
2. `npm install --dev`
3. `./create_pages.sh` (first time only, or when you create/delete tabs)
4. Launch a local server on port 8000 with `gatsby develop`

### Launching your own website

**IMPORTANT**: before launching your website using GitLab Pages,
be sure to read the GitLab Pages Terms and Conditions.
There are usage and content restrictions that may disallow you from using GitLab Pages
to host your website.

Suppose your GitLab project is `my_site` and your username is `my_name`.

1. Set up GitLab pages for `my_site`
1. Change the website path prefix in `gatsby-config.js` to `my_site`
1. Ensure GitLab CI is set to run
1. Run the CI pipeline against `master`
1. Boom. You should have a website at `my_name.gitlab.io/my_site`

The CI pipeline should run with every merge request.
The pipeline will automatically run against master and update your site
after you merge a merge request.
There's also a test stage for non-master branches which will attempt to build the site.
We recommend you set GitLab to block merge requests unless the pipeline succeeds.

### Customization

The Getting Started guide on our [website][wtp_website]
can help you gather the specs for your new website.
Please use that guide to collect information about your website.
Gathering specs probably the most time-consuming part of this whole project,
but you'll need that info to continue.

Things that need to be adjusted for your use case:

```shell
# Website path prefix
# Site metadata (title, author, keywords, etc.)
# User preferences (colors, splash images, etc.)
# Purchase options
gatsby-config.js

# User content: tabs, footer, splash section
user-content.jsx

# Icon and banner logo images
# If you change file names or types, Change imports in src/components/ files
# The banner logo is optional. It should be very wide, like 2000x250
# icon.png should be a 512x512 logo
# Customize how the icon and banner appear in the splash section of user-content.jsx
src/images/icon.png
src/images/banner_logo.png

# Splash images
# Create one or more splash images to make an impact.
# Splash images should be large and wide, like 2000x1000
# List your splash images in user-content.jsx
src/images/splash_image_1.jpg
src/images/splash_image_2.jpg

# Items
# Place item yamls in content/items/
```

### Using your own domain or subdomain

If you're deploying through GitLab Pages,
follow the instructions in
[GitLab's documenation for using a custom domain][gitlab_pages_custom_domain].

Note that the setup may differ depending on who you own your domain through.
For example, in the case of Google Domains,
you don't need a name for the `A` and `TXT` entries for a domain,
but you need something for `CNAME` and `TXT` for subdomain entries.
What to put in those fields for a subomain isn't quite what you see in GitLab's docs:
you need to leave out the `.example.com` part.

### E-commerce and Stripe integration

For e-commerce (or just product listings),
you can use [Stripe][stripe].

1. Create a Stripe account
1. Set your Stripe account to client-only mode
1. Input your product info on Stripe (see below)
1. Set your `STRIPE_SECRET_KEY` and `STRIPE_PUBLISHABLE_KEY` in the `.env.*` files.
1. Set the `purchase` field in `user-content.jsx` if you want to sell products.
1. Place a `Products` component to list your items.

Note that `.env.*` files are ignored by git by default.
Don't put your secret keys in an insecure location!

Because `GraphQL` is picky,
you'll need to ensure that products have all required fields.
Items with missing fields won't appear in your product list.
Required fields are:

- Name
- Description
- Price
- Image
- `keywords` Metadata field as a comma-separated list of keywords

[osr_website]: https://opensocialresources.com/
[wtp_website]: https://website-template.opensocialresources.com/
[changelog]: CHANGELOG.md
[code_of_conduct]: CODE_OF_CONDUCT.md
[gatsby]: https://www.gatsbyjs.org/
[gitlab_pages_custom_domain]: https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html
[stripe]: https://stripe.com/
