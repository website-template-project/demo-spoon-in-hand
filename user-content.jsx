/* User defined data
 *
 * Input user data here.
 * Ensure each item is defined, at least as an empty string.
 */
import React from "react"
import styled, { keyframes } from "styled-components"
import { Breakpoints } from "./src/util/breakpoints"
import useSiteMetadata from "./src/hooks/use-site-metadata"
import useUserPreferences from "./src/hooks/use-user-preferences"

import NoteBox from "./src/components/note-box"
import LocalProducts from "./src/components/local-products"
import GoogleMap from "./src/components/google-map"

import DynamicImage from "./src/components/dynamic-image"
import SlideShow from "./src/components/slideshow"
import TabList from "./src/components/tab-list"
import Title from "./src/components/title"

/**********************************************************************
 * Input client website customization below
 **********************************************************************/

const footer = (<>
  <p>
    All content on this website,
    including the Website Template Project logo,
    is &copy; 2020&ndash;2021&nbsp;
    <a href="https://opensocialresources.com/" target="_blank" rel="noreferrer">
      Open Social Resources
    </a>. All rights reserved.
  </p>
</>)

/* Tab info and settings
 * Any tabs you want.
 *
 * For special tabs and components, you'll need to adjust imports in this file
 *
 * Use ProgressiveImage for images over img
 */

const  aboutTab = {
  slug: "about",
  name: "About us",
  content: (<>
    <h2>About Us</h2>
    <NoteBox type="note" content={(<div>
      This is not a real restaurant.
      This demo website was built using the Website Template Project
      by Open Social Resources.
      Please visit us at&nbsp;
      <a href="https://website-template.opensocialresources.com">
        website-template.opensocialresources.com</a>
    </div>)} />
    <p>
      At the Spoon in Hand restaurant,
      we've proudly served home-style meals to
      the Detroit community since 1998.
      Come and have a seat at our table,
      where you'll find rich meats, savory curry, and bright soup.
    </p>

    <h4>Phone</h4>
    <p>123-456-7890</p>
    <h4>Hours</h4>
    <ul>
      <li>Tuesday-Sunday: 11 AM &ndash; 9 PM</li>
      <li>Monday: Closed</li>
    </ul>
    <h4>Location</h4>
    <p>1234 Some Street. Detroit, MI. 48201.</p>
    <GoogleMap src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2949.5134055941053!2d-83.09662308416101!3d42.331576279188894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883b32ba305433a1%3A0x70da09958baf802c!2sMike&#39;s%20Famous%20Ham%20Place!5e0!3m2!1sen!2sus!4v1600166816699!5m2!1sen!2sus" />
  </>),
}

const menuTab = {
  slug: "menu",
  name: "Menu",
  content: (<>
    <h2>Menu</h2>
    <NoteBox type="note" content={(<div>
      This is not a real restaurant.
      This demo website was built using the Website Template Project
      by Open Social Resources.
      Please visit us at&nbsp;
      <a href="https://website-template.opensocialresources.com">
        website-template.opensocialresources.com</a>
    </div>)} />
    <p>
      Please let us know if you have any dietary restrictions.
      Most of our meals can be made to accommodate
      vegan, gluten-free, nut-free, and soy-free diets.
    </p>

    <h4>Appetizers</h4>
    <LocalProducts filter={product => product.keywords.includes("appetizer")} />

    <h4>Entr&eacute;es</h4>
    <LocalProducts filter={product => product.keywords.includes("entree")} />
  </>),
}

const tabs = [
  aboutTab,
  menuTab,
]

/*******************************************************************************
 *
 * Business Splash
 *
 * Defined here to handle a lot of customization.
 * Consider relocating to components once we have themes.
 *
 ******************************************************************************/

const Separator = styled.hr`
  border-top: 1px solid lightgray;
`

// Need to move the id marker up so navigation is visible
// Use only if bottom tab list present
const TabContentPlaceholder = styled.div`
  position: relative;
  top: -3em;
`
const StyledBusinessIdentifiers = styled.div`
  width: 100%;
  display: inline-grid;
  box-sizing: border-box;
  grid-template-columns: ${props => props.shouldUseIconLogo ?
    'min-content auto' :
    'auto'
  };
  column-gap: 15px;
  justify-content: start;
  align-items: center;
  grid-template-areas: ${props => props.shouldUseIconLogo ?
    '"icon title" "description description"' :
    '"title" "description"'
  };

  @media ${Breakpoints.smallOrLarger} {
    padding: 15px;
    padding-top: 0;
    row-gap: 7px;
    grid-template-columns: min-content auto min-content;
    grid-template-areas:
      "icon        title"
      "icon        description";
  }
`

const TitleContainer = styled.div`
  min-width: 60vw;
  grid-area: title;
`

const IconContainer = styled.div`
  grid-area: icon;
  height: 70px;
  width: 70px;

  @media ${Breakpoints.smallOrLarger} {
    height: 120px;
    width: 120px;
    padding-right: 15px;
    margin-bottom: 8px;
  }
`

const SlideshowContainer = styled.div`
  background-color: var(--backgroundColor);
  @media ${Breakpoints.smallOrLarger} {
    margin: auto;
    max-width: 90%;
  }
  @media ${Breakpoints.largeOrLarger} {
    max-width: 80%;
  }
`

const slideLeft = keyframes`
  from {
    margin-left: 100%;
    width: 300%;
  }
  to {
    margin-left: 0%;
    width: 100%;
  }
`

const DescriptionContainer = styled.p`
  grid-area: description;
  font-style: italic;
  font-size: larger;
  animation: ${slideLeft} 1s ease-out;

  @media ${Breakpoints.smallOrLarger} {
    margin: 0;
    vertical-align: middle;
  }
`

function BusinessIdentifiers(props) {
  const siteMetadata = useSiteMetadata()
  return (
    <StyledBusinessIdentifiers
      shouldUseIconLogo={true}
      data-qa={props["data-qa"] || "BusinessIdentifiers"}
    >

      <IconContainer data-qa={"BusinessIcon"}>
        <DynamicImage
          src="icon.png"
          alt="Business icon"
          data-qa="BusinessIcon"
          shouldShowBackground={false}
        />
      </IconContainer>

      <TitleContainer>
      {/*
        <h1>
          <DynamicImage
            src="banner_logo.png"
            alt={siteMetadata.title}
            data-qa="BusinessBanner"
            shouldShowBackground={false}
          />
        </h1>
      */}
        <Title
          text={siteMetadata.title}
          data-qa={"BusinessTitle"}
        />
      </TitleContainer>

      <DescriptionContainer data-qa={"BusinessDescription"}>
        {siteMetadata.description}
      </DescriptionContainer>

    </StyledBusinessIdentifiers>

  )
}

function TabPageSplash(props) {
  const userPreferences = useUserPreferences()

  return (<>
    <BusinessIdentifiers />
    <SlideshowContainer>
      <SlideShow
        images={userPreferences.splashImages}
        data-qa={"SplashImages"}
      />
    </SlideshowContainer>


    {/* Ensure bottom tab list is visible when switching tabs
        Only use with bottom tab list
    */}
    <TabContentPlaceholder id="TabContent" />

    <div role={"navigation"}>
      <TabList
        activeTab={props.activeTab}
        tabs={tabs}
      />
    </div>

    <Separator />
  </>)
}

export default function getUserContent() {
  return {
    footer,
    tabs,
    BusinessIdentifiers,
    TabPageSplash,
  }
}
