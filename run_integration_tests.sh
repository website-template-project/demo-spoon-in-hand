#!/bin/sh -e

#docker-compose up \
#    --exit-code-from \
#    test-runner

ORIGIN="${PWD}"
cd "$(dirname $0)"

if [ "${1}" = "--build" ]; then
    shift
    ./create_pages.sh
    ./sitemap.sh
    gatsby clean
    gatsby build
fi
gatsby serve &
GATSBY_PROD_SERVER=$!
TEST_SERVER=http://localhost:9000 \
    npm --prefix "$(dirname $0)/integration_tests" run test || \
    echo "Tests failed!" 1>&2
kill ${GATSBY_PROD_SERVER}
cd "${ORIGIN}"
