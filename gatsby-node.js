const path = require(`path`)
const stripeItem = require("./src/util/stripe-items").stripeItem
const localItem = require("./src/util/local-items").localItem
const getStripeData = require("./src/util/stripe-queries").getStripeData
const getLocalItemData = require("./src/util/local-item-queries").getLocalItemData

exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions

  const itemTypes = [
    `ItemsYaml`,
    `StripePrice`,
  ]

  if (itemTypes.includes(node.internal.type)) {
    let id
    if (node.internal.type === `StripePrice`) {
      id = node.product.id
    } else {
      id = node.id
    }
    createNodeField({
      node,
      name: `path`,
      value: `/items/${id}/`
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  // **Note:** The graphql function call returns a Promise
  // see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise for more info
  const localItemQueryData = await getLocalItemData(graphql)
  const stripeItemQueryData = await getStripeData(graphql)
  const localItems = []
  const stripeItems = []
  for (const node of localItemQueryData.data.allItemsYaml.nodes) {
    localItems.push(localItem(node))
  }
  for (const node of stripeItemQueryData.data.allStripePrice.nodes) {
    stripeItems.push(stripeItem(node))
  }
  const items = localItems.concat(stripeItems)

  for (const item of items) {
    createPage({
      path: item.path,
      component: path.resolve(`./src/layout/item-page.jsx`),
      context: {
        item: item,
      }
    })
  }

}
