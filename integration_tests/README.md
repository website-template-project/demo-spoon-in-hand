# Integration testing

## Running integration tests

### Using docker (simpler)

To run integration tests using docker,
install `docker` and `docker-compose`,
then:

```shell
cd [PROJECT_DIR]
./run_integration_tests.sh
```

Sometimes, you may need to rebuild images
if they are using outdated code:

```shell
docker-compose build test-runner
docker-compose build test-server
```

### Without docker

To run integration tests without docker,
you'll need this setup.

1. Install `geckodriver` and put it in the path. See the Dockerfile.
1. `npm install` in both the project dir and `integration_tests/`.

Running the tests:

1. Start the prod server with `gatsby clean && gatsby build && gatsby serve`
1. `cd integration_tests`
1. `npm run test`
