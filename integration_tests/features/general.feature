# features/general.feature

@test_env
Feature: Testing Features on the /general/ page
  Background:
    Given I visit the "/general/" page
  
  Scenario: Links to tabs work
    When I click the "items" "TabLink"
    Then I am on the "/items#TabContent" page
