const { Given, When, Then } = require("@cucumber/cucumber")
const { until, By } = require("selenium-webdriver")
const assert = require("assert").strict
const { findQa, findXpath } = require("./util")

Given(`I visit the {string} page`, async function (page) {
  const url = `${this.siteUrl}${page}`
  // Visit the page
  await this.driver.get(url)
  await this.driver.wait(until.urlIs(url))
})

When(`I click the {string}`, async function (buttonDataQa) {
  const button = await findQa(this.driver, buttonDataQa)
  // Ensure the button is clickable
  this.driver.wait(until.elementIsVisible(button))
  await button.click()
})

When(`I click the {string} {string}`, async function (elementText, elementDataQa) {
  const matchingElement = await findXpath(
    this.driver,
    `//*[@data-qa="${elementDataQa}" and text()="${elementText}"]`,
  )
  // Ensure element is clickable
  this.driver.wait(until.elementIsVisible(matchingElement))
  // Click it
  await matchingElement.click()
})

Then(`I am on the {string} page`, async function (expectedPage) {
  // Wait to be on the correct page
  // That's enough to validate, but the failure output lacks the actual URL
  // Ensure informative output from assert
  const expectedUrl = `${this.siteUrl}${expectedPage}`
  try {
    await this.driver.wait(until.urlIs(expectedUrl), this.defaultTimeout - 1000)
  } finally {
    const actualUrl = await this.driver.getCurrentUrl()
    assert.equal(actualUrl, expectedUrl)
  }
})

Then(`the {string} is selected`, async function (elementDataQa) {
  const element = await findQa(this.driver, elementDataQa)
  // Ensure that element is selected
  this.driver.wait(until.elementIsSelected(element))
})
