#!/bin/sh

USER_CONTENT="user-content.jsx"
SITEMAP_DIR="static"
SITEMAP_FILE="${SITEMAP_DIR}/sitemap.xml"

site_url() {
    url="$(grep 'const shareURL =' "${USER_CONTENT}" | cut -d\" -f2)"
    echo ${url%/}
}

tab_names() {
    # This is not ideal but works without a bunch of fancy stuff
    grep '^  slug: ".*",$' "${USER_CONTENT}" | cut -d\" -f2
}

url_info() {
    path="${1}"
    # Priority should range from 0.0 to 1.0
    # See https://www.sitemaps.org/protocol.html
    priority="${2}"
    url="$(site_url)"
cat << EOF
    <url>
        <loc>${url}/${path}</loc>
        <lastmod>${today}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>${priority}</priority>
    </url>
EOF
}

tab_url_info() {
    for tab in "" $(tab_names); do
        [ -z "${tab}" ] || tab="${tab}"
        url_info "${tab}/" "1.0"
    done
}

item_url_info() {
    tab=""
    while IFS= read -r line; do
        tab_candidate="$(echo "${line}" | grep '^  slug: ".*",$' | cut -d\" -f2)"
        item="$(echo "${line}" | grep '^ *itemId: ".*",$' | cut -d\" -f2)"
        if [ -n "${tab_candidate}" ]; then
            tab="${tab_candidate}"
        elif [ -n "${item}" ]; then
            url_info "${tab}/?item=${item}" "0.5"
        fi
    done < "${USER_CONTENT}"
}

create_sitemap() {
    url="$(site_url)"
    today="$(date '+%Y-%m-%d')"
    echo '<?xml version="1.0" encoding="UTF-8"?>' > "${SITEMAP_FILE}"
    echo "<urlset xmlns=\"${url}\">" >> "${SITEMAP_FILE}"
    tab_url_info >> "${SITEMAP_FILE}"
    item_url_info >> "${SITEMAP_FILE}"
    echo '</urlset>' >> "${SITEMAP_FILE}"
}

[ -f "${USER_CONTENT}" ] || { echo "Unable to find '${USER_CONTENT}'" 1>&2 ; exit 1; }
[ -d "${SITEMAP_DIR}" ] || { echo "Unable to find directory '${SITEMAP_DIR}'" 1>&2 ; exit 1; }

create_sitemap
